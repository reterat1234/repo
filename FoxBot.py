import telebot
from telebot import types
import GeneralConfig as config
import UserConfig as user
import LogCore
import AnswersCore as ans


LogCore.CheckAllFiles()
LogCore.ParseLogFileXLSX()
LogCore.GetFileIndex()
user.DownloadUsers()

bot = telebot.TeleBot(config.TOKEN)

#Поиск пользователя в системе, создание его личных настроек в случае если поиск не наешл ничего + возвращает позицию в массиве пользовательский настроек
def UserIdentification(message):
    key = user.SearchUserID(message.chat.id) 
    if  key == -1:
        user.AddUser(message.chat.id)
        return len(user.UsersData) - 1
    else:                            
        return key

#обобщенная функция добавления нового лога
def AddLog(message, text, filename,):
    LogCore.AddNewLog(message.from_user.first_name, message.from_user.last_name, message.chat.username, message.from_user.id, text, filename, message.chat.id)
    if LogCore.WriteNewLog() == False:
        print("The log is not written. The file Info.xlsx is being occupied by another process, free it.")

#Вывод главной клавиатуры
def SetMainKeyboard(message, text):
    keyboard = types.ReplyKeyboardMarkup(True)
    keyboard.row('Что умеет этот бот?')
    keyboard.row('Инструкция работы с ботом')
    #keyboard.row('Сделать пожертвование')
    keyboard.row('Получить денежное вознаграждение')
    bot.send_message(message.chat.id, text,  reply_markup = keyboard)

#Отправка новостей всем юзерам бота (кроме себя самого и заблоченных)
def SendMessageAll(text, id):
    for i in range(len(user.UsersData)):
        if(user.UsersData[i].UserID != id and user.UsersData[i].Status != -1):
            bot.send_message(user.UsersData[i].UserID, text)

#Финальный этап регистрации
def FinishRegist(message, KeyUserSettings):
    user.UsersData[KeyUserSettings].BotMode = 0
    bot.send_message(message.chat.id, ans.RegistarationAnswers(4))
    AddLog(message, "Регистрационные данные. Номер телефона: " + user.UsersData[KeyUserSettings].phone + " Номер карты: " + user.UsersData[KeyUserSettings].bank + " Геолокация: " + user.UsersData[KeyUserSettings].location , "")


@bot.message_handler(commands=['start'])
def startBot(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == -1):
        return 0
    SetMainKeyboard(message, ans.StartMessage())

#хелпик для админов
@bot.message_handler(commands=['adminHelp'])
def AdminHelp(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == -1):
        return 0

    if(user.UsersData[KeyUserSettings].Status == 1):
        bot.send_message(message.chat.id, ans.AdminHelpAnswers())

#Трансформация админа в обычного юзера
@bot.message_handler(commands=['IamUser'])
def IamUser(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == 1):
        user.UsersData[KeyUserSettings].Status = 0
        user.UpgradeUsersFile()
        bot.send_message(message.chat.id, ans.YouUsersAnswers())

#Отправить новость всем юзерам (первая часть, начало сбора инфы для новости)
@bot.message_handler(commands=['SendNews'])
def SendNewsAll(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == 1):
        user.UsersData[KeyUserSettings].BotMode = 2
        bot.send_message(message.chat.id, ans.SendNewsAnswers(1))

#Добавить юзера в черный  список
@bot.message_handler(commands=['AddToBlackList'])
def AddToBlackList(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == 1):
        user.UsersData[KeyUserSettings].BotMode = 3
        bot.send_message(message.chat.id, ans.BlackListAnswers(1))

#Просмотр списка пользователей бота
@bot.message_handler(commands=['ViewUsersList'])
def ViewUsersList(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == 1):
        bot.send_message(message.chat.id, user.GetUserList())

#Удалить юзера из черного  списка
@bot.message_handler(commands=['RemoveFromBlackList'])
def RemoveFromBlackList(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == 1):
        user.UsersData[KeyUserSettings].BotMode = 4
        bot.send_message(message.chat.id, ans.BlackListAnswers(5))

#Отправить новость конкретному юзеру (первая часть, запрос id)
@bot.message_handler(commands=['SendNewsToUser'])
def SendNewsToUser(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == 1):
        user.UsersData[KeyUserSettings].BotMode = 5
        bot.send_message(message.chat.id, ans.SendNewsAnswers(3))

#Отправить файл логов
@bot.message_handler(commands=['GetLogsFile'])
def GetLogsFile(message):
    try:
        KeyUserSettings = UserIdentification(message)
        if(user.UsersData[KeyUserSettings].Status == 1):
            bot.send_document(message.chat.id, open(config.PathLogFile, 'rb'))
    except Exception as e:
            bot.send_message(message.chat.id, ans.GetFileAnswers())

#Старт регистрации
@bot.message_handler(commands=['registration', 'reregist'])
def Registration(message):
    KeyUserSettings = UserIdentification(message)
    user.UsersData[KeyUserSettings].BotMode = 7
    bot.send_message(message.chat.id, ans.RegistarationAnswers(1))




#Отловка отправленных текстовых данных
@bot.message_handler(content_types=['text'])
def handler_text(message):

    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == -1):
        return 0

    # Перехват скрытой команды для запуска процесса входа в админ.панель
    if(message.text == "iAMadmin"):
        if(user.UsersData[KeyUserSettings].Status == 0):
            bot.send_message(message.chat.id, ans.YouAdminAnswers(1))
            user.UsersData[KeyUserSettings].BotMode = 1
        else:
            bot.send_message(message.chat.id, ans.YouAdminAnswers(2))
        return 0 

    # Перехват пароля для входа в админ.панель
    if(user.UsersData[KeyUserSettings].BotMode == 1):
        user.UsersData[KeyUserSettings].BotMode = 0
        if(message.text == config.AdminPass):
            bot.send_message(message.chat.id, ans.AdminPasswordAnswers(2))
            user.UsersData[KeyUserSettings].Status = 1
            user.UpgradeUsersFile()
        else:
            bot.send_message(message.chat.id, ans.AdminPasswordAnswers(1))
        return 0 


    # Перехват текста для будущей пубуликации в боте (для всех кроме заблоченных)
    if(user.UsersData[KeyUserSettings].BotMode == 2 and user.UsersData[KeyUserSettings].Status == 1):
        user.UsersData[KeyUserSettings].MyMessage = message.text
        user.UsersData[KeyUserSettings].BotMode = 0
        SendMessageAll(user.UsersData[KeyUserSettings].MyMessage, user.UsersData[KeyUserSettings].UserID)
        bot.send_message(message.chat.id, ans.SendNewsAnswers(2))
        return 0


    # Перехват id юзера для будущего сообщения от лица бота
    if(user.UsersData[KeyUserSettings].BotMode == 5 and user.UsersData[KeyUserSettings].Status == 1):
        try:
            User_key = user.SearchUserID(int(message.text))
            if(KeyUserSettings != User_key):
                if(User_key != -1):
                    config.tempIDuser = int(message.text)
                    user.UsersData[KeyUserSettings].BotMode = 6
                    bot.send_message(message.chat.id, ans.SendNewsAnswers(4))
                else:
                    bot.send_message(message.chat.id, ans.BlackListAnswers(3))
            else:
                bot.send_message(message.chat.id, ans.SendNewsAnswers(6))
        except Exception as e:
            bot.send_message(message.chat.id, ans.InvalidIdAnswers())
            user.UsersData[KeyUserSettings].BotMode = 0
        return 0

    # Перехват текста для будущего сообщения от лица бота конкретному пользователю
    if(user.UsersData[KeyUserSettings].BotMode == 6 and user.UsersData[KeyUserSettings].Status == 1):
        user.UsersData[KeyUserSettings].BotMode = 0
        bot.send_message(config.tempIDuser, message.text)
        config.tempIDuser = 0
        bot.send_message(message.chat.id, ans.SendNewsAnswers(5))
        return 0



    # Перехват id юзера для добавления или удаления из черного списка
    if(user.UsersData[KeyUserSettings].BotMode >= 3 and user.UsersData[KeyUserSettings].BotMode <= 4 and user.UsersData[KeyUserSettings].Status == 1):
        try:
            User_key = user.SearchUserID(int(message.text))
            if(KeyUserSettings != User_key):
                if(User_key != -1):
                    if(user.UsersData[KeyUserSettings].BotMode == 3):
                        bot.send_message(message.chat.id, ans.BlackListAnswers(2))
                        user.UsersData[User_key].Status = -1
                    else:
                        bot.send_message(message.chat.id, ans.BlackListAnswers(6))
                        user.UsersData[User_key].Status = 0
                    user.UpgradeUsersFile()
                else:
                    bot.send_message(message.chat.id, ans.BlackListAnswers(3))
            else:
                bot.send_message(message.chat.id, ans.BlackListAnswers(4))
            user.UsersData[KeyUserSettings].BotMode = 0
        except Exception as e:
            bot.send_message(message.chat.id, ans.InvalidIdAnswers())
        return 0


    # Перехват номера телефона (регистрация)
    if(user.UsersData[KeyUserSettings].BotMode == 7):
        user.UsersData[KeyUserSettings].BotMode = 8
        user.UsersData[KeyUserSettings].phone = str(message.text)
        bot.send_message(message.chat.id, ans.RegistarationAnswers(2))
        return 0

    # Перехват банковского счета (регистрация)
    if(user.UsersData[KeyUserSettings].BotMode == 8):
        user.UsersData[KeyUserSettings].BotMode = 9
        user.UsersData[KeyUserSettings].bank = str(message.text)
        bot.send_message(message.chat.id, ans.RegistarationAnswers(3))
        return 0

    # Перехват геолокации (если текстом) (регистрация)
    if(user.UsersData[KeyUserSettings].BotMode == 9):
        user.UsersData[KeyUserSettings].location = str(message.text)
        FinishRegist(message, KeyUserSettings)
        return 0




    if(message.text == "Что умеет этот бот?"):
        bot.send_message(message.chat.id, ans.InfoAnswers())
    elif(message.text == "Инструкция работы с ботом"):
        bot.send_message(message.chat.id, ans.InstructionAnswers())
    elif(message.text == "Получить денежное вознаграждение"):
        bot.send_message(message.chat.id, ans.MoneyInfoAnswers())
    #elif(message.text == "Сделать пожертвование"):
    #    bot.send_message(message.chat.id, ans.DonatAnswers())
    else:
        message.text = LogCore.ExcelFormulaAnalysis(message.text)
        AddLog(message, message.text, "")
        bot.reply_to(message, ans.InfoReceivedAnswers())
        bot.send_message(message.chat.id, ans.ThankAnswers())


#Отловка отправленных документов (+ текстовые подписи к ним)
@bot.message_handler(content_types=['document'])
def handle_document(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == -1):
        return 0

    try:
        file_info = bot.get_file(message.document.file_id)
        downloaded_file = bot.download_file(file_info.file_path)

        src = 'files/' + message.document.file_name;
        with open(src, 'wb') as new_file:
            new_file.write(downloaded_file)

        if message.caption is not None:
            message.caption = LogCore.ExcelFormulaAnalysis(message.caption)
        AddLog(message, message.caption, message.document.file_name)
        bot.reply_to(message, ans.InfoReceivedAnswers())
        bot.send_message(message.chat.id, ans.ThankAnswers())
    except Exception as e:
        if(str(e) == "A request to the Telegram API was unsuccessful. Error code: 400. Description: Bad Request: file is too big"):
            bot.send_message(message.chat.id, ans.ExcepFileBigAnswers(1))
        else:
            print(e)


#Отловка отправленных фоточек (+ текстовые подписи к ним)
@bot.message_handler(content_types=['photo'])
def handle_photo(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == -1):
        return 0

    try:
        file_info = bot.get_file(message.photo[0].file_id)
        downloaded_file = bot.download_file(file_info.file_path)

        src = 'files/' + str(config.fileIndex) + ".jpg"
        with open(src, 'wb') as new_file:
            new_file.write(downloaded_file)

        if message.caption is not None:
            message.caption = LogCore.ExcelFormulaAnalysis(message.caption)
        AddLog(message, message.caption, str(config.fileIndex) + ".jpg")
        LogCore.UpIndex()
        bot.reply_to(message, ans.InfoReceivedAnswers())
        bot.send_message(message.chat.id, ans.ThankAnswers())
        bot.send_message(message.chat.id, ans.CorrectAnswers())
    except Exception as e:
        if(str(e) == "A request to the Telegram API was unsuccessful. Error code: 400. Description: Bad Request: file is too big"):
            bot.send_message(message.chat.id, ans.ExcepFileBigAnswers(2))
        else:
            print(e)


#Отловка отправленных видеороликов и гифок (+ текстовые подписи к ним)
@bot.message_handler(content_types=['video', 'animation'])
def handle_video(message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == -1):
        return 0

    try:
        idFile = 0
        if message.content_type == 'video':
            idFile = message.video.file_id
        elif message.content_type == 'animation':
            idFile = message.animation.file_id

        file_info = bot.get_file(idFile)
        fileType = file_info.file_path.split('.')[len(file_info.file_path.split('.'))-1]
        with open('files/' + str(config.fileIndex) + "." + fileType, "wb") as f:
            file_content = bot.download_file(file_info.file_path)
            f.write(file_content)

        if message.caption is not None:
            message.caption = LogCore.ExcelFormulaAnalysis(message.caption)
        AddLog(message, message.caption, str(config.fileIndex) + "." + fileType)
        LogCore.UpIndex()
        bot.reply_to(message, ans.InfoReceivedAnswers())
        bot.send_message(message.chat.id, ans.ThankAnswers())
    except Exception as e:
        if(str(e) == "A request to the Telegram API was unsuccessful. Error code: 400. Description: Bad Request: file is too big"):
            bot.send_message(message.chat.id, ans.ExcepFileBigAnswers(1))
        else:
            print(e)


#Отловка отправленных геолокаций
@bot.message_handler(content_types=['location'])
def handle_location (message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == -1):
        return 0

    # Перехват геолокации (если текстом) (регистрация)
    if(user.UsersData[KeyUserSettings].BotMode == 9):
        user.UsersData[KeyUserSettings].location = str(message.location.latitude) + "," + str(message.location.longitude)
        FinishRegist(message, KeyUserSettings)
        return 0

    if message.location is not None:
        AddLog(message, "Геолокация: " + str(message.location.latitude) + "," + str(message.location.longitude), "")
        LogCore.UpIndex()
        bot.reply_to(message, ans.InfoReceivedAnswers())
        bot.send_message(message.chat.id, ans.ThankAnswers())

#Отловка всего прочего
@bot.message_handler(content_types=["audio", "sticker", "video_note", "voice", "contact",
                 "new_chat_members", "left_chat_member", "new_chat_title", "new_chat_photo", "delete_chat_photo",
                 "group_chat_created", "supergroup_chat_created", "channel_chat_created", "migrate_to_chat_id",
                 "migrate_from_chat_id", "pinned_message"])
def handle_location (message):
    KeyUserSettings = UserIdentification(message)
    if(user.UsersData[KeyUserSettings].Status == -1):
        return 0

    bot.reply_to(message, ans.NotParsAnswers())

bot.polling( none_stop = True )