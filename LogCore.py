import os
import openpyxl 
import datetime
import GeneralConfig as config

#парсинг файлового индекса
def GetFileIndex():
    f = open(config.FileIndexName, 'r')
    config.fileIndex = int(f.read())
    f.close()

# определение пути к всем системным файлам
def CheckAllFiles():
    config.PathLogFile = os.path.abspath(config.LogFileName)
    config.PathUsersFile = os.path.abspath(config.FileUsers)

# класс для хранения запарсенных логов
class LogXLSX(object):
 
    def __init__(self, namePage):
        self.nameFilePage = namePage
        self.listData = []
        self.listName = []
        self.listLastName = []
        self.listUsername = []
        self.listId = []
        self.listUserText = []
        self.listFileName = []
        self.listChatId = []

    # парсинг одной странички файла логов
    def ParseFile(self, nPage):
        File = openpyxl.open(config.PathLogFile, read_only = True)
        Page = File.worksheets[nPage]
        try:
            for row in range(1, Page.max_row+1):
                self.listData.append(Page[row][0].value)
                self.listName.append(Page[row][1].value)
                self.listLastName.append(Page[row][2].value)
                self.listUsername.append(Page[row][3].value)
                self.listId.append(Page[row][4].value)
                self.listUserText.append(Page[row][5].value)
                self.listFileName.append(Page[row][6].value)
                self.listChatId.append(Page[row][7].value)
        except IndexError:
            CreateVoidFileXLSX()

Logs = LogXLSX("Логи")

#объявление оглавлений на странице екселя
def SetHeader(Page):
    Page['A1'] = 'Дата'
    Page['B1'] = 'Имя'
    Page['C1'] = 'Фамилия'
    Page['D1'] = 'username'
    Page['E1'] = 'id юзера'
    Page['F1'] = 'Написал боту'
    Page['G1'] = 'Добавил в бота файл'
    Page['H1'] = 'id чата'

#запись в ексель
def WriteNewLog():
    try:
        File = openpyxl.Workbook()
 
        Page = File.active
        Page.title = "Логи"
        SetHeader(Page)

        for i in range(2, len(Logs.listData)+1):
            Page[i][0].value = Logs.listData[i-1]

        for i in range(2, len(Logs.listName)+1):
            Page[i][1].value = Logs.listName[i-1]

        for i in range(2, len(Logs.listLastName)+1):
            Page[i][2].value = Logs.listLastName[i-1]

        for i in range(2, len(Logs.listUsername)+1):
            Page[i][3].value = Logs.listUsername[i-1]

        for i in range(2, len(Logs.listId)+1):
            Page[i][4].value = Logs.listId[i-1]

        for i in range(2, len(Logs.listUserText)+1):
            Page[i][5].value = Logs.listUserText[i-1]

        for i in range(2, len(Logs.listFileName)+1):
            Page[i][6].value = Logs.listFileName[i-1]

        for i in range(2, len(Logs.listChatId)+1):
            Page[i][7].value = Logs.listChatId[i-1]

        File.save(config.PathLogFile)
        File.close()
        return True
    except Exception as e:
        return False

#добавление новых данных в структуры 
def AddNewLog(name, lastName, username, id, text, fileName, chat_id):
    Logs.listData.append(datetime.datetime.now().strftime("%d-%m-%Y %H:%M"))
    Logs.listName.append(name)
    Logs.listLastName.append(lastName)
    if username is not None:
        Logs.listUsername.append("@" + username)
    Logs.listId.append(id)
    Logs.listUserText.append(text)
    Logs.listFileName.append(fileName)
    Logs.listChatId.append(chat_id)

#парсинг xlsx файла
def ParseLogFileXLSX():
    if os.path.exists(config.PathLogFile) == True:
        Logs.ParseFile(0)
    else:
        CreateVoidFileXLSX()
        print("File has been recreated")

#создание пустого Excel-файла
def CreateVoidFileXLSX():
    File = openpyxl.Workbook()
    Page = File.active
    Page.title = "Логи"
    SetHeader(Page)
    File.save(config.PathLogFile)
    File.close()
    ParseLogFileXLSX()

#Увеличение файлового индекса
def UpIndex():
    config.fileIndex = config.fileIndex +  1
    f = open(config.FileIndexName, 'w')
    f.write(str(config.fileIndex))
    f.close()

#Проверка на эксель формулу
def ExcelFormulaAnalysis(str):
    if(str[0] == "="):
        str = str.replace("=", " ")
    return str
