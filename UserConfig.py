import GeneralConfig as config
import os

UsersData = []

#Класс личных настроек пользователя
class Users(object):
    def __init__(self, id):
        self.UserID = id
        self.Status = 0         #-1 - заблокированный пользователь
                                # 0 - дефолтный статус юзера
                                # 1 - статуст админа

        self.BotMode = 0        # 0 - дефолтный режим системы, бот работает со стандартным функционалом (опрос инфы, заход в админ.панель, выдача методическо-описательной инфы)
                                # 1 - бот в режиме запроса пароля для входа в админку
                                # 2 - бот в режиме запроса информации для новости
                                # 3 - бот в режиме запроса id юзера, которого нужно добавить в черный список
                                # 4 - бот в режиме запроса id юзера, которого нужно удалить из черного списока
                                # 5 - бот в режиме запроса id юзера, которому нужно отправить сообщение
                                # 6 - бот в режиме запроса информации для новости для выбранного юзера
                                # 7 - режим регистрации (запрос номера)
                                # 8 - режим регистрации (запрос банковского счета)
                                # 9 - режим регистрации (запрос локации)

        self.MyMessage = ""     #Поле хранения текста будущей новости (доступно только для админов)
        self.phone = ""         #Телефон (регистрация)
        self.bank = ""          #Счет (регистрация)
        self.location = ""      #Геолокация (регистрация)

#Добавление нового пользователя в систему
def AddUser(id):
    try:
        UsersData.append(Users(id))
        file = open(config.FileUsers, 'a', encoding='utf-8')
        if(len(UsersData) == 1):
            file.write(str(id))
        else:
            file.write("\n" + str(id))
        file.write("\n0")
        file.close()
    except Exception as e:
        print(e)

#Поиск пользователя в системе
def SearchUserID(id):
    key = 0
    for i in UsersData:
        if i.UserID == id:
            return key
        key = key + 1
    return -1

#Обновление системного файла со списком пользователей
def UpgradeUsersFile():
    try:
        file = open(config.FileUsers, 'w', encoding='utf-8')
        for i in range(len(UsersData)):
            if(i == 0):
                file.write(str(UsersData[i].UserID))
            else:
                file.write("\n" + str(UsersData[i].UserID))
            file.write("\n" + str(UsersData[i].Status))
        file.close()
    except Exception as e:
        print(e)

#Подгрузка уже зарегестрированных пользователей
def DownloadUsers():
    try:
        if os.path.exists(config.PathUsersFile) == True:
            file = open(config.FileUsers, "r")
            countUsers = 0
            while True:
                line = file.readline()
                if not line:
                    break
                UsersData.append(Users(int(line)))
                line = file.readline()
                UsersData[countUsers].Status = int(line)
                countUsers = countUsers + 1
    except Exception as e:
        print(e)

#Получение текстовой расшифровки кода статуса юзера
def GetUserStatus(number_case):
    status = {
        -1: ' (заблокированный)',
         0: ' ',
         1: ' (админ)'
    }
    return status[number_case]

#Получение текстового списка пользователей
def GetUserList():
    list = "Список пользователей:\n"
    for i in range(len(UsersData)):
        list = list + str(UsersData[i].UserID) + GetUserStatus(UsersData[i].Status) + "\n"
    return list
